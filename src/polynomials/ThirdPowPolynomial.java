package polynomials;

public class ThirdPowPolynomial implements Polynomial{

    /**
     * В задаче требуется задать не более 5 полиномов третьей степени
     */
    private double c0;                  // свободный коэффициент
    private double c1;                  // коэффициент при x
    private double c2;                  // коэффициент при x^2
    private double c3;                  // коэффициент при x^3

    private final int polymonialCase;       // выбор полинома из заранее предложенных

    public ThirdPowPolynomial(int polymonialCase) {
        this.polymonialCase = polymonialCase;
    }

    @Override
    public void getCoefficient() {
        switch(polymonialCase) {
            case 1 ->  {
                setC0(-17);
                setC1(7);
                setC2(-4);
                setC3(3);
            }
            case 2 -> {
                setC0(21);
                setC1(-3);
                setC2(-5);
                setC3(2);
            }
            case 3 -> {
                setC0(-9);
                setC1(5);
                setC2(-3);
                setC3(2);
            }
            case 4 -> {
                setC0(-12);
                setC1(-3);
                setC2(2);
                setC3(1);
            }
            case 5 -> {
                setC0(-19);
                setC1(6);
                setC2(-3);
                setC3(1);
            }
        }
    }

    public double getC0() {
        return c0;
    }

    public void setC0(double c0) {
        this.c0 = c0;
    }

    public double getC1() {
        return c1;
    }

    public void setC1(double c1) {
        this.c1 = c1;
    }

    public double getC2() {
        return c2;
    }

    public void setC2(double c2) {
        this.c2 = c2;
    }

    public double getC3() {
        return c3;
    }

    public void setC3(double c3) {
        this.c3 = c3;
    }
}
