package dataworker;

import java.util.Scanner;

public class InputReader implements DataReader {

    final Scanner scanner;

    /**
     * data[0] - содержит в себе значение левой границы
     * data[1] - содержит в себе значение правой границы
     * data[2] - содержит в себе значение число разбиений
     * data[3] - содержит в себе значение наперёд заданной точности(EPS)
     */
    double[] data = new double[5];

    public InputReader(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public double[] getData() throws Exception {
        for (int i = 0; i < 4; i++) {
            if (scanner.hasNextDouble() || scanner.hasNextInt()) {
                data[i] = scanner.nextDouble();
            }
        }
        if (data[0] > data[1]) {
            throw new Exception("Левая граница \"правее\" правой границы\n" +
                    "=======================================================");
        }
        if (data[2] < 0 ) {
            throw new Exception("Число разбиений должно быть положительным\n" +
                    "=======================================================");
        }
        if (data[3] < 0) {
            throw new Exception("Наперёд заданная точность должна быть больше нуля\n" +
                    "=======================================================");
        }
        return data;
    }
}

