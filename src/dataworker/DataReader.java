package dataworker;

public interface DataReader {
    double[] getData() throws Exception;
}
