import dataworker.CaseReader;
import dataworker.InputReader;
import methods.SimpsonsMethod;
import methods.TrapezeMethod;
import polynomials.ThirdPowPolynomial;

import java.util.Scanner;

public class ImproperIntegrals {
    public static void main(String[] args) {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите 1 - если вы хотите посчитать интеграл от 1/x");
            System.out.println("Введите 2 - если вы хотите посчитать интеграл от кусочно заданной функции\n" +
                    "                y = 2, если x < 2\n" +
                    "                y = x - 2, елси x >= 2");
            CaseReader caseReader = new CaseReader(scanner, 1, 2);
            int functionalCase = 1;
            try {
                functionalCase = caseReader.getCase();
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            MainInformationPrinter informationPrinter = new MainInformationPrinter();
            System.out.println(informationPrinter.inputInformation());
            InputReader inputReader = new InputReader(scanner);

            ThirdPowPolynomial polynomial = new ThirdPowPolynomial(1);

            double[] data;
            double leftBound = 0;
            double rightBound = 0;
            long partition = -1;
            double EPS = 0;

            try {
                data = inputReader.getData();
                leftBound = data[0];
                rightBound = data[1];
                partition = (long) data[2];
                EPS = data[3];
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            switch (functionalCase) {
                case 1 -> {
                    if (leftBound <= rightBound || partition != -1 || EPS != 0) {
                        double result;
                        if (leftBound < 0 && rightBound > 0) {
                            SimpsonsMethod first = new SimpsonsMethod(polynomial, leftBound, 0 - EPS, EPS, partition / 2);
                            SimpsonsMethod second = new SimpsonsMethod(polynomial, 0 + EPS, rightBound, EPS, partition / 2);
                            first.solve();
                            second.solve();
                            result = first.getRoot() + second.getRoot();
                        } else if (leftBound < 0 && rightBound == 0) {
                            SimpsonsMethod first = new SimpsonsMethod(polynomial, leftBound, 0 - EPS, EPS, partition / 2);
                            first.solve();
                            result = first.getRoot();
                        } else if (leftBound == 0 && rightBound > 0) {
                            SimpsonsMethod second = new SimpsonsMethod(polynomial, 0 + EPS, rightBound, EPS, partition / 2);
                            second.solve();
                            result = second.getRoot();
                        } else {
                            SimpsonsMethod method = new SimpsonsMethod(polynomial, leftBound, rightBound, EPS, partition);
                            method.solve();
                            result = method.getRoot();
                        }

                        System.out.println(result);
                    }
                }
                case 2 -> {
                    double result;

                    TrapezeMethod method = new TrapezeMethod(polynomial, leftBound, rightBound, EPS, partition);
                    method.solve();
                    result = method.getRoot();

                    System.out.println(result);
                }
            }
        }
    }
}
