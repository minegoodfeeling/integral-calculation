package methods;

import polynomials.ThirdPowPolynomial;

public class NewtonKotessMethod {

    /**
     * Метод написан для n = 8;
     */

    private final ThirdPowPolynomial polynomial;
    private final double leftBound;
    private final double rightBound;

    private final long partition = 8;

    private double integralSum = 0;

    public NewtonKotessMethod(ThirdPowPolynomial polynomial1, double leftBound, double rightBound) {
        this.polynomial = polynomial1;
        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }


    public void solve() {
        double b = rightBound;
        double a = leftBound;
        double c08 = 989 * (b - a) / 28350;
        double c88 = c08;
        double c18 = 5888 * (b - a) / 28350;
        double c78 = c18;
        double c28 = -928 * (b - a) / 28350;
        double c68 = c28;
        double c38 = 10496 * (b - a) / 28350;
        double c58 = c38;
        double c48 = -4540 * (b - a) / 28350;

        double step = (b - a) / partition;

        integralSum = c08 * f(a) + c18 * f(a + step) + c28 * f(a + step * 2) +
                c38 * f(a + 3 * step) + c48 * f(a + 4 * step) +
                c58 * f(a + 5 * step) + c68 * f(a + 6 * step) +
                c78 * f(a + 7 * step) + c88 * f(b);

    }

    public double getRoot() {
        return integralSum;
    }


    private double f(double x) {
        return polynomial.getC0() + polynomial.getC1() * x
                + polynomial.getC2() * x * x + polynomial.getC3() * x * x * x;

    }

}
