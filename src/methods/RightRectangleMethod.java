package methods;

import polynomials.ThirdPowPolynomial;

public class RightRectangleMethod implements Method {
    private final ThirdPowPolynomial polynomial;
    private final double leftBound;                                 // левая граница вычисления интеграла
    private final double rightBound;                                // правая граница вычисления интеграла

    private final double EPS;                                       // наперёд заданная точность

    private int counterOfIteration;
    private final int maxIt = 100;                                  // максимальное число итераций

    private double integralSum = 0;
    private long partition;                                         // число разбиений


    public RightRectangleMethod(ThirdPowPolynomial polynomial, double leftBound, double rightBound, double EPS, long partition) {
        this.polynomial = polynomial;
        this.leftBound = leftBound;
        this.rightBound = rightBound;
        this.EPS = EPS;
        this.partition = partition;
    }


    @Override
    public void solve() {
        integralSum = rawSolve(partition);
        partition = partition * 2;
        double integralSumHatch = rawSolve(partition);
        while (Math.abs(integralSum - integralSumHatch) / 3 >= 0.000001 && counterOfIteration < maxIt) {
            partition = partition * 2;
            integralSum = integralSumHatch;
            integralSumHatch = rawSolve(partition);
            counterOfIteration++;
        }
    }

    private double rawSolve(long ownPartition) {
        double step = (rightBound - leftBound) / ownPartition;
        double left = leftBound;
        double right = rightBound;
        double rawIntegralSum = 0;

        while (Math.abs(right - left) >= EPS * 10) {
            rawIntegralSum += f(left + step) * step;
            left += step;
        }
        return rawIntegralSum;
    }

    @Override
    public double getRoot() {
        return integralSum;
    }

    @Override
    public int getCounterOfIteration() {
        return counterOfIteration;
    }

    private double f(double x) {
        return polynomial.getC0() + polynomial.getC1() * x
                + polynomial.getC2() * x * x + polynomial.getC3() * x * x * x;

    }
}
