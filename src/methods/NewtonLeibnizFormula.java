package methods;

import polynomials.ThirdPowPolynomial;

public class NewtonLeibnizFormula implements Method{
    private final ThirdPowPolynomial polynomial;
    private final double leftBound;                                 // левая граница вычисления интеграла
    private final double rightBound;                                // правая граница вычисления интеграла

    private final double EPS;                                       // наперёд заданная точность

    private int counterOfIteration = 1;
    private final int maxIt = 20;                                   // максимальное число итераций

    private double integralSum = 0;
    private long partition;                                         // число разбиений


    public NewtonLeibnizFormula(ThirdPowPolynomial polynomial, double leftBound, double rightBound, double EPS, long partition) {
        this.polynomial = polynomial;
        this.leftBound = leftBound;
        this.rightBound = rightBound;
        this.EPS = EPS;
        this.partition = partition;
    }

    @Override
    public void solve() {
        integralSum = integralOfF(rightBound) - integralOfF(leftBound);
    }

    @Override
    public double getRoot() {
        return integralSum;
    }

    @Override
    public int getCounterOfIteration() {
        return counterOfIteration;
    }

    private double f(double x) {
        return polynomial.getC0() + polynomial.getC1() * x
                + polynomial.getC2() * x * x + polynomial.getC3() * x * x * x;

    }

    private double integralOfF(double x) {
        return polynomial.getC0() * x + polynomial.getC1() * x * x / 2
                + polynomial.getC2() * x * x * x / 3 +
                polynomial.getC3() * x * x * x * x / 4;
    }
}
