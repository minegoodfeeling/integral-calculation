package methods;

import ComplexFunction.HyperbolaFuntion;
import polynomials.ThirdPowPolynomial;

public class SimpsonsMethod implements Method{
    private final ThirdPowPolynomial polynomial;
    private final double leftBound;                                 // левая граница вычисления интеграла
    private final double rightBound;                                // правая граница вычисления интеграла

    private final double EPS;                                       // наперёд заданная точность

    private int counterOfIteration;
    private final int maxIt = 1;                                  // максимальное число итераций

    private double integralSum = 0;
    private long partition;                                         // число разбиений


    public SimpsonsMethod(ThirdPowPolynomial polynomial, double leftBound, double rightBound, double EPS, long partition) {
        this.polynomial = polynomial;
        this.leftBound = leftBound;
        this.rightBound = rightBound;
        this.EPS = EPS;
        this.partition = partition;
    }
    @Override
    public void solve() {
        integralSum = rawSolve(partition);
        partition = partition * 2;
        double integralSumHatch = rawSolve(partition);
        while (Math.abs(integralSum - integralSumHatch) / 3 >= 0.000001 && counterOfIteration < maxIt) {
            partition = partition * 2;
            integralSum = integralSumHatch;
            integralSumHatch = rawSolve(partition);
            counterOfIteration++;
        }
    }

    public double rawSolve(double ownPartition) {
        double left = leftBound;
        double right = rightBound;
        double step = (rightBound - leftBound) / ownPartition;
        double rawIntegralSum = 0;

        for(int i = 0; i < partition; i++) {
            if(i == 0) {
                rawIntegralSum += f(left);
            } else if (i == partition - 1) {
                rawIntegralSum += f(right);
            } else if (i % 2 != 0) {
                rawIntegralSum += 4 * f(left + step * i);
            } else if (i % 2 == 0) {
                rawIntegralSum += 2 * f(left + step * i);
            }
        }

        return rawIntegralSum * step / 3;
    }

    @Override
    public double getRoot() {
        return integralSum;
    }

    @Override
    public int getCounterOfIteration() {
        return counterOfIteration;
    }

//    private double f(double x) {
//        return polynomial.getC0() + polynomial.getC1() * x
//                + polynomial.getC2() * x * x + polynomial.getC3() * x * x * x;
//
//    }

    private double f( double x) {
        return 1 / x;
    }
}
