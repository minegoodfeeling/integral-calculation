package methods;

import polynomials.ThirdPowPolynomial;

public class TrapezeMethod implements Method{

    private final ThirdPowPolynomial polynomial;
    private final double leftBound;                                 // левая граница вычисления интеграла
    private final double rightBound;                                // правая граница вычисления интеграла

    private final double EPS;                                       // наперёд заданная точность

    private int counterOfIteration;
    private final int maxIt = 1;                                   // максимальное число итераций

    private double integralSum = 0;
    private long partition;                                         // число разбиений


    public TrapezeMethod(ThirdPowPolynomial polynomial, double leftBound, double rightBound, double EPS, long partition) {
        this.polynomial = polynomial;
        this.leftBound = leftBound;
        this.rightBound = rightBound;
        this.EPS = EPS;
        this.partition = partition;
    }
    @Override
    public void solve() {
        integralSum = rawSolve(partition);
        partition = partition * 2;
        double integralSumHatch = rawSolve(partition);
        while (Math.abs(integralSum - integralSumHatch) / 3 >= 0.000001 && counterOfIteration < maxIt) {
            partition = partition * 2;
            integralSum = integralSumHatch;
            integralSumHatch = rawSolve(partition);
            counterOfIteration++;
        }
    }

    private double rawSolve(long ownPartition) {
        double left = leftBound;
        double right = rightBound;
        double step = (rightBound - leftBound) / ownPartition;
        double rawIntegralSum = 0;

        while(Math.abs(right - left) >= EPS * 10) {
            rawIntegralSum += step * (f(left) + f(left + step));
            left += step;
        }
        return rawIntegralSum / 2;
    }

    @Override
    public double getRoot() {
        return integralSum;
    }

    @Override
    public int getCounterOfIteration() {
        return counterOfIteration;
    }

//    private double f(double x) {
//        return polynomial.getC0() + polynomial.getC1() * x
//                + polynomial.getC2() * x * x + polynomial.getC3() * x * x * x;
//
//    }

    private double f(double x) {
        if( x < 2) {
            return 2;
        } else {
            return x - 2;
        }
    }
}
