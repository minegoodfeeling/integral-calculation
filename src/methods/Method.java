package methods;

public interface Method {
    void solve();
    double getRoot();
    int getCounterOfIteration();
}
