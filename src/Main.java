import dataworker.CaseReader;
import dataworker.InputReader;
import dataworker.DataReader;
import methods.*;
import polynomials.ThirdPowPolynomial;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        while (true) {
            Scanner scanner = new Scanner(System.in);
            MainInformationPrinter informationPrinter = new MainInformationPrinter();
            System.out.println(informationPrinter.typeOfIntegral());

            CaseReader polymonialCase = new CaseReader(scanner, 1, 5);
            int variantOfPolynomial;
            try {
                variantOfPolynomial = polymonialCase.getCase();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }

            System.out.println(informationPrinter.inputInformation());
            DataReader reader = new InputReader(scanner);

            Method method = null;
            double[] data;
            double leftBound;
            double rightBound;
            long partition;
            double EPS;
            try {
                data = reader.getData();
                leftBound = data[0];
                rightBound = data[1];
                partition = (long) data[2];
                EPS = data[3];
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }

            ThirdPowPolynomial polynomial = new ThirdPowPolynomial(variantOfPolynomial);
            polynomial.getCoefficient();

            System.out.println(informationPrinter.typeOfMethod());
            CaseReader methodCase = new CaseReader(scanner, 1, 8);

            int variantOfMethod = 1;
            try {
                variantOfMethod = methodCase.getCase();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }

            // Для вычислительной задачи:
            NewtonKotessMethod newtonKotessMethod = null;
            switch (variantOfMethod) {
                case 1 -> method = new LeftRectangleMethod(polynomial, leftBound, rightBound, EPS, partition);
                case 2 -> method = new MidRectangleMethod(polynomial, leftBound, rightBound, EPS, partition);
                case 3 -> method = new RightRectangleMethod(polynomial, leftBound, rightBound, EPS, partition);
                case 4 -> method = new TrapezeMethod(polynomial, leftBound, rightBound, EPS, partition);
                case 5 -> method = new SimpsonsMethod(polynomial, leftBound, rightBound, EPS, partition);
                case 6 -> newtonKotessMethod = new NewtonKotessMethod(polynomial, leftBound, rightBound);
                default -> {
                    System.out.println("Используем формулу Ньютона-Лейбница!\n");
                    method = new NewtonLeibnizFormula(polynomial, leftBound, rightBound, EPS, partition);
                }
            }

            // Для вычислительной задачи;
            if (newtonKotessMethod != null) {
                newtonKotessMethod.solve();
                double root = newtonKotessMethod.getRoot();
                System.out.println("Интеграл равен: " + root);
            }

            if (method != null) {
                method.solve();
                double root = method.getRoot();
                int counterOfIteration = method.getCounterOfIteration();
                System.out.println("Интеграл равен: " + root);
                System.out.println("Число итераций равно: " + counterOfIteration);
            }
            System.out.println(informationPrinter.continueOrExitInformation());
            CaseReader continueOrExitCase = new CaseReader(scanner, 1, 2);
            try {
                int varietyOfContinueOrExitCase = continueOrExitCase.getCase();
                if (varietyOfContinueOrExitCase == 2) {
                    System.out.println(". . . Завершение программы . . .");
                    break;
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
            }
        }
    }
}
