package ComplexFunction;

public interface ComplexFunction {
    double f(double x);
}
