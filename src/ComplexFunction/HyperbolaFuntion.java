package ComplexFunction;

public class HyperbolaFuntion implements ComplexFunction{
    @Override
    public double f(double x) {
        return 1 / x;
    }
}
